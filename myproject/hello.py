from flask import Flask
from elasticapm.contrib.flask import ElasticAPM
app = Flask(__name__)
app.config['ELASTIC_APM']={
    'SERVICE_NAME': 'testFlask',
    'SECRET_TOKEN': '',
    'SERVER_URL': 'http://localhost:8200'
}
apm = ElasticAPM(app)
@app.route('/')
def hello_world():
    return 'Hello, World!'